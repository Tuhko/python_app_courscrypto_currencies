from django.shortcuts import render, HttpResponse, redirect

import api


def redirect_home(request):
    return redirect("home", days_range=30, currencies="ETH")


# Create your views here.

# Pour un simple message :
def dashboard_2(request):
    return HttpResponse("<h1> Hello World !! </h1")


# Pour un template :
def dashboard(request, days_range=30, currencies="ETH"):
    days, rates = api.get_rates(currencies=currencies.split(","), days=days_range)
    page_label = {7: "Semaine", 30: "Mois", 150: "5 mois"}.get(days_range, "Personnalisé")

    return render(request, "devise/index.html", context={"data": rates,
                                                         "days_labels": days,
                                                         "currencies": currencies,
                                                         "page_label": page_label})
