from datetime import datetime, timedelta
from pprint import pprint

import requests


def get_rates(currencies, days=30):
    end_date = datetime.today()
    start_date = end_date - timedelta(days=days)

    # Correspond à la monnaie dans laquelle seront affichés les cours
    base = 'EUR'

    # Créé un str à partir d'une liste de Tokens fournis en paramètres.
    join_symbols = ','.join(currencies)

    # You can switch source data between (default) forex, bank view or crypto currencies.
    source = 'crypto'

    query = f"http://api.exchangerate.host/timeseries?" \
            f"start_date={start_date}&end_date={end_date}&base={base}&symbols={join_symbols}&source={source}"

    r = requests.get(query)
    if not r and not r.json():
        return False, False

    # Liste des jours pris en compte pour l'affichage (rappel 10 par défaut, days=10)
    api_rates = r.json().get('rates')

    # Créé n dict, avec comme clé, la currency et en valeur, une liste vide
    all_rates = {currency: [] for currency in currencies}

    # La liste des jours, triée.
    all_days = sorted(api_rates.keys())

    for each_day in all_days:
        [all_rates[currency].append(rate) for currency, rate in api_rates[each_day].items()]

    # pprint(all_rates)

    return all_days, all_rates


if __name__ == '__main__':
    days, rates = get_rates(currencies=["ETH"])
    # pprint(days)
    # pprint(rates)
